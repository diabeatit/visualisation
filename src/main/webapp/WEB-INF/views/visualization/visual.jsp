
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<!-- initialize the chart api -->

<script type="text/javascript"> 

 google.load('visualization', '1.0', {'packages':['corechart']}); 
 google.setOnLoadCallback(drawChart); 
 
 function drawChart() {  
                         
         var patient_data = new google.visualization.DataTable();                
         patient_data.addColumn('date', 'Record Date');
         patient_data.addColumn('number', 'Blood Glucose'); 
         patient_data.addColumn('number', 'Insulin'); 
         patient_data.addRows([          
                      [new Date(2012,9,6),7,13],
                      [new Date(2012,9,5),23,3],
                      [new Date(2012,9,4),14,43],
                      [new Date(2012,9,3),7,23],
                      [new Date(2012,9,2),57,33]

         ]); 
         
         var patient_options = {'title':'Patient Info', 
                          vAxis: {title: 'Unit',  titleTextStyle: {color: 'black'}},
                         'width':500,                       
                         'height':400}; 
         var patient_chart = new google.visualization.LineChart(document.getElementById('patient_chart')); 
         patient_chart .draw(patient_data, patient_options); 
         
                                        
                        
                
         var workout_data = google.visualization.arrayToDataTable([ 
                                ['Date', 'Workout'], 
                                ['2012-9-3',  123],['2012-9-4',  67],['2012-9-5',  84]
                                 ]); 
         var workout_chart_options = {'title':'Workout information', 
                         vAxis: {title: 'Time',  titleTextStyle: {color: 'black'}},
                         'width':500,                       
                         'height':400}; 
         var workout_chart = new google.visualization.ColumnChart(document.getElementById('workout_chart')); 
         workout_chart.draw(workout_data, workout_chart_options);   
         
         
         
         
         google.load('visualization', '1.0', {'packages':['corechart']}); 
         google.setOnLoadCallback(drawChart); 
         
       
                              
        	 var data = new google.visualization.DataTable();                 
              
        	 data.addColumn('string', 'Energy');  
        	 data.addColumn('number', 'Calorie');  
        	 data.addRows([         
        				['Intake', 300], 
        				['Consuming', 179], 
                 ]); 
                 
        	 var options = {'title':'Calorie',                        
        			 'width':500,                       
        			 'height':400};  
        	 var chart = new google.visualization.PieChart(document.getElementById('chart_div')); 
        	 chart.draw(data, options);        
                 
         
         
 }
 

</script>


<script type="text/javascript">
$(document).ready(function(){
        
	 $("#chart_div").hide();
	 $("#workout_chart").hide();
	 
        });
</script>


<script type="text/javascript">
$(document).ready(function(){
        $("#piechartButton").click(function(){
        $("#patient_chart").hide();	
        $("#workout_chart").hide();
        $("#chart_div").show();
        });
        });
</script>

<script type="text/javascript">
$(document).ready(function(){
        $("#linechartButton").click(function(){
        	
       
        $("#chart_div").hide();
        $("#workout_chart").hide();
        $("#patient_chart").show();
        });
        });
</script>

<script type="text/javascript">
$(document).ready(function(){
        $("#barchartButton").click(function(){
        	
        $("#chart_div").hide();
        $("#patient_chart").hide();
        $("#workout_chart").show();
       
        });
        });
</script>


<title>View the workout records</title>
</head>

<body>
<div id="patient_chart"></div> 
<div id="workout_chart"></div>
<div id="chart_div"></div> 
<input type="button" id="linechartButton" value="Line Chart">
<input type="button" id="piechartButton" value="Pie Chart">
<input type="button" id="barchartButton" value="Bar Chart">

${ number}
</body>
</html>
 
