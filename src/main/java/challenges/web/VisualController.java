package challenges.web;


import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import challenges.domain.MetricGoalStatus;




@RequestMapping("/visualization")
@Controller
@RooWebScaffold(path = "visualization", formBackingObject = MetricGoalStatus.class)
public class VisualController {
	
	@RequestMapping(value = "/chart{id_}", produces = "text/html")
	public String getlala(@PathVariable("id_") Long id_, Model uiModel)
	{
		double i = 3.4;
		
		uiModel.addAttribute("insulin", i);
		//uiModel.addAttribute("calorie", i);
		//uiModel.addAttribute("blood", i);
		
		
		return "visualization/visual";
	}
}