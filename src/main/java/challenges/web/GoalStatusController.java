package challenges.web;

import challenges.domain.GoalStatus;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/goalstatuses")
@Controller
@RooWebScaffold(path = "goalstatuses", formBackingObject = GoalStatus.class)
@RooWebFinder
public class GoalStatusController {
}
