package org.apache.jsp.WEB_002dINF.views;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class menu_jspx extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(3);
    _jspx_dependants.add("/WEB-INF/tags/menu/menu.tagx");
    _jspx_dependants.add("/WEB-INF/tags/menu/category.tagx");
    _jspx_dependants.add("/WEB-INF/tags/menu/item.tagx");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.release();
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted.release();
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<div version=\"2.0\" id=\"menu\">");
      if (_jspx_meth_menu_005fmenu_005f0(_jspx_page_context))
        return;
      out.write("</div>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_menu_005fmenu_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:menu
    org.apache.jsp.tag.web.menu.menu_tagx _jspx_th_menu_005fmenu_005f0 = new org.apache.jsp.tag.web.menu.menu_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fmenu_005f0);
    _jspx_th_menu_005fmenu_005f0.setJspContext(_jspx_page_context);
    // /WEB-INF/views/menu.jspx(5,60) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fmenu_005f0.setZ("nZaf43BjUg1iM0v70HJVEsXDopc=");
    // /WEB-INF/views/menu.jspx(5,60) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fmenu_005f0.setId("_menu");
    _jspx_th_menu_005fmenu_005f0.setJspBody(new Helper( 0, _jspx_page_context, _jspx_th_menu_005fmenu_005f0, null));
    _jspx_th_menu_005fmenu_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fmenu_005f0);
    return false;
  }

  private boolean _jspx_meth_menu_005fcategory_005f0(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:category
    org.apache.jsp.tag.web.menu.category_tagx _jspx_th_menu_005fcategory_005f0 = new org.apache.jsp.tag.web.menu.category_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f0);
    _jspx_th_menu_005fcategory_005f0.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fcategory_005f0.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(6,75) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f0.setZ("ZEqiWpKr5zoInq7gRbU6wAAGL7k=");
    // /WEB-INF/views/menu.jspx(6,75) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f0.setId("c_challenger");
    _jspx_th_menu_005fcategory_005f0.setJspBody(new Helper( 1, _jspx_page_context, _jspx_th_menu_005fcategory_005f0, null));
    _jspx_th_menu_005fcategory_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f0);
    return false;
  }

  private boolean _jspx_meth_sec_005fauthorize_005f0(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec:authorize
    org.springframework.security.taglibs.authz.JspAuthorizeTag _jspx_th_sec_005fauthorize_005f0 = (org.springframework.security.taglibs.authz.JspAuthorizeTag) _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.get(org.springframework.security.taglibs.authz.JspAuthorizeTag.class);
    _jspx_th_sec_005fauthorize_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sec_005fauthorize_005f0.setParent(new javax.servlet.jsp.tagext.TagAdapter((javax.servlet.jsp.tagext.SimpleTag) _jspx_parent));
    // /WEB-INF/views/menu.jspx(7,69) name = ifAnyGranted type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sec_005fauthorize_005f0.setIfAnyGranted("ROLE_ANONYMOUS,ROLE_ADMIN");
    int _jspx_eval_sec_005fauthorize_005f0 = _jspx_th_sec_005fauthorize_005f0.doStartTag();
    if (_jspx_eval_sec_005fauthorize_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_meth_menu_005fitem_005f0(_jspx_th_sec_005fauthorize_005f0, _jspx_page_context))
        return true;
    }
    if (_jspx_th_sec_005fauthorize_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.reuse(_jspx_th_sec_005fauthorize_005f0);
      throw new SkipPageException();
    }
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.reuse(_jspx_th_sec_005fauthorize_005f0);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f0 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f0);
    _jspx_th_menu_005fitem_005f0.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f0.setParent(_jspx_th_sec_005fauthorize_005f0);
    // /WEB-INF/views/menu.jspx(8,138) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f0.setZ("C8bksAcx/Z/to2gMXLmGBEOivWQ=");
    // /WEB-INF/views/menu.jspx(8,138) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f0.setUrl("/challengers?form");
    // /WEB-INF/views/menu.jspx(8,138) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f0.setMessageCode("global_menu_new");
    // /WEB-INF/views/menu.jspx(8,138) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f0.setId("i_challenger_new");
    _jspx_th_menu_005fitem_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f0);
    return false;
  }

  private boolean _jspx_meth_sec_005fauthorize_005f1(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec:authorize
    org.springframework.security.taglibs.authz.JspAuthorizeTag _jspx_th_sec_005fauthorize_005f1 = (org.springframework.security.taglibs.authz.JspAuthorizeTag) _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted.get(org.springframework.security.taglibs.authz.JspAuthorizeTag.class);
    _jspx_th_sec_005fauthorize_005f1.setPageContext(_jspx_page_context);
    _jspx_th_sec_005fauthorize_005f1.setParent(new javax.servlet.jsp.tagext.TagAdapter((javax.servlet.jsp.tagext.SimpleTag) _jspx_parent));
    // /WEB-INF/views/menu.jspx(10,54) name = ifAllGranted type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sec_005fauthorize_005f1.setIfAllGranted("ROLE_ADMIN");
    int _jspx_eval_sec_005fauthorize_005f1 = _jspx_th_sec_005fauthorize_005f1.doStartTag();
    if (_jspx_eval_sec_005fauthorize_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_meth_menu_005fitem_005f1(_jspx_th_sec_005fauthorize_005f1, _jspx_page_context))
        return true;
    }
    if (_jspx_th_sec_005fauthorize_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted.reuse(_jspx_th_sec_005fauthorize_005f1);
      throw new SkipPageException();
    }
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted.reuse(_jspx_th_sec_005fauthorize_005f1);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f1 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f1);
    _jspx_th_menu_005fitem_005f1.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f1.setParent(_jspx_th_sec_005fauthorize_005f1);
    // /WEB-INF/views/menu.jspx(11,189) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f1.setZ("hV7T3eBkHIWsO5ycjfTEF9JntgU=");
    // /WEB-INF/views/menu.jspx(11,189) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f1.setUrl((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("/challengers?page=1&size=${empty param.size ? 10 : param.size}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/menu.jspx(11,189) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f1.setMessageCode("global_menu_list");
    // /WEB-INF/views/menu.jspx(11,189) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f1.setId("i_challenger_list");
    _jspx_th_menu_005fitem_005f1.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f1);
    return false;
  }

  private boolean _jspx_meth_sec_005fauthorize_005f2(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec:authorize
    org.springframework.security.taglibs.authz.JspAuthorizeTag _jspx_th_sec_005fauthorize_005f2 = (org.springframework.security.taglibs.authz.JspAuthorizeTag) _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.get(org.springframework.security.taglibs.authz.JspAuthorizeTag.class);
    _jspx_th_sec_005fauthorize_005f2.setPageContext(_jspx_page_context);
    _jspx_th_sec_005fauthorize_005f2.setParent(new javax.servlet.jsp.tagext.TagAdapter((javax.servlet.jsp.tagext.SimpleTag) _jspx_parent));
    // /WEB-INF/views/menu.jspx(13,64) name = ifAnyGranted type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sec_005fauthorize_005f2.setIfAnyGranted("ROLE_ADMIN,ROLE_USER");
    int _jspx_eval_sec_005fauthorize_005f2 = _jspx_th_sec_005fauthorize_005f2.doStartTag();
    if (_jspx_eval_sec_005fauthorize_005f2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_meth_menu_005fitem_005f2(_jspx_th_sec_005fauthorize_005f2, _jspx_page_context))
        return true;
    }
    if (_jspx_th_sec_005fauthorize_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.reuse(_jspx_th_sec_005fauthorize_005f2);
      throw new SkipPageException();
    }
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.reuse(_jspx_th_sec_005fauthorize_005f2);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f2(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f2 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f2);
    _jspx_th_menu_005fitem_005f2.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f2.setParent(_jspx_th_sec_005fauthorize_005f2);
    // /WEB-INF/views/menu.jspx(14,171) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f2.setZ("2j0hqM0fFbmYQjg4AWzGygeQ6UE=");
    // /WEB-INF/views/menu.jspx(14,171) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f2.setUrl("/challengers?find=ByEmailEquals&form");
    // /WEB-INF/views/menu.jspx(14,171) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f2.setMessageCode("global_menu_find");
    // /WEB-INF/views/menu.jspx(14,171) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f2.setId("fi_challenger_emailequals");
    _jspx_th_menu_005fitem_005f2.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f2);
    return false;
  }

  private boolean _jspx_meth_sec_005fauthorize_005f3(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec:authorize
    org.springframework.security.taglibs.authz.JspAuthorizeTag _jspx_th_sec_005fauthorize_005f3 = (org.springframework.security.taglibs.authz.JspAuthorizeTag) _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.get(org.springframework.security.taglibs.authz.JspAuthorizeTag.class);
    _jspx_th_sec_005fauthorize_005f3.setPageContext(_jspx_page_context);
    _jspx_th_sec_005fauthorize_005f3.setParent(new javax.servlet.jsp.tagext.TagAdapter((javax.servlet.jsp.tagext.SimpleTag) _jspx_parent));
    // /WEB-INF/views/menu.jspx(17,60) name = ifAnyGranted type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sec_005fauthorize_005f3.setIfAnyGranted("ROLE_USER,ROLE_ADMIN");
    int _jspx_eval_sec_005fauthorize_005f3 = _jspx_th_sec_005fauthorize_005f3.doStartTag();
    if (_jspx_eval_sec_005fauthorize_005f3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_meth_menu_005fcategory_005f1(_jspx_th_sec_005fauthorize_005f3, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fcategory_005f2(_jspx_th_sec_005fauthorize_005f3, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fcategory_005f3(_jspx_th_sec_005fauthorize_005f3, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fcategory_005f4(_jspx_th_sec_005fauthorize_005f3, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fcategory_005f5(_jspx_th_sec_005fauthorize_005f3, _jspx_page_context))
        return true;
    }
    if (_jspx_th_sec_005fauthorize_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.reuse(_jspx_th_sec_005fauthorize_005f3);
      throw new SkipPageException();
    }
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAnyGranted.reuse(_jspx_th_sec_005fauthorize_005f3);
    return false;
  }

  private boolean _jspx_meth_menu_005fcategory_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:category
    org.apache.jsp.tag.web.menu.category_tagx _jspx_th_menu_005fcategory_005f1 = new org.apache.jsp.tag.web.menu.category_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f1);
    _jspx_th_menu_005fcategory_005f1.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fcategory_005f1.setParent(_jspx_th_sec_005fauthorize_005f3);
    // /WEB-INF/views/menu.jspx(18,73) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f1.setZ("ZGg6S94nj2yOTfEjpUU31l6c1d0=");
    // /WEB-INF/views/menu.jspx(18,73) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f1.setId("c_goal");
    _jspx_th_menu_005fcategory_005f1.setJspBody(new Helper( 2, _jspx_page_context, _jspx_th_menu_005fcategory_005f1, null));
    _jspx_th_menu_005fcategory_005f1.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f1);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f3(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f3 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f3);
    _jspx_th_menu_005fitem_005f3.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f3.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(19,126) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f3.setZ("ZscBQkzR927MS40xerMiIiIo7tc=");
    // /WEB-INF/views/menu.jspx(19,126) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f3.setUrl("/goals?form");
    // /WEB-INF/views/menu.jspx(19,126) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f3.setMessageCode("global_menu_new");
    // /WEB-INF/views/menu.jspx(19,126) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f3.setId("i_goal_new");
    _jspx_th_menu_005fitem_005f3.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f3);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f4(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f4 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f4);
    _jspx_th_menu_005fitem_005f4.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f4.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(20,177) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f4.setZ("aqKrCDpjMBCc3W4H9uVhqHNoUQc=");
    // /WEB-INF/views/menu.jspx(20,177) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f4.setUrl((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("/goals?page=1&size=${empty param.size ? 10 : param.size}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/menu.jspx(20,177) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f4.setMessageCode("global_menu_list");
    // /WEB-INF/views/menu.jspx(20,177) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f4.setId("i_goal_list");
    _jspx_th_menu_005fitem_005f4.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f4);
    return false;
  }

  private boolean _jspx_meth_menu_005fcategory_005f2(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:category
    org.apache.jsp.tag.web.menu.category_tagx _jspx_th_menu_005fcategory_005f2 = new org.apache.jsp.tag.web.menu.category_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f2);
    _jspx_th_menu_005fcategory_005f2.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fcategory_005f2.setParent(_jspx_th_sec_005fauthorize_005f3);
    // /WEB-INF/views/menu.jspx(22,85) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f2.setZ("qIUTu0U8mDKIYZ8fzXkXQ2oy4Ag=");
    // /WEB-INF/views/menu.jspx(22,85) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f2.setId("c_metricgoalstatus");
    _jspx_th_menu_005fcategory_005f2.setJspBody(new Helper( 3, _jspx_page_context, _jspx_th_menu_005fcategory_005f2, null));
    _jspx_th_menu_005fcategory_005f2.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f2);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f5(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f5 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f5);
    _jspx_th_menu_005fitem_005f5.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f5.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(23,151) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f5.setZ("GDP4fFn3z6DJCxzyy/3lKKrgmoo=");
    // /WEB-INF/views/menu.jspx(23,151) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f5.setUrl("/metricgoalstatuses?form");
    // /WEB-INF/views/menu.jspx(23,151) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f5.setMessageCode("global_menu_new");
    // /WEB-INF/views/menu.jspx(23,151) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f5.setId("i_metricgoalstatus_new");
    _jspx_th_menu_005fitem_005f5.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f5);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f6(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f6 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f6);
    _jspx_th_menu_005fitem_005f6.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f6.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(24,202) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f6.setZ("YK6kqHQevH8mT876nFFaSOq81G8=");
    // /WEB-INF/views/menu.jspx(24,202) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f6.setUrl((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("/metricgoalstatuses?page=1&size=${empty param.size ? 10 : param.size}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/menu.jspx(24,202) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f6.setMessageCode("global_menu_list");
    // /WEB-INF/views/menu.jspx(24,202) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f6.setId("i_metricgoalstatus_list");
    _jspx_th_menu_005fitem_005f6.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f6);
    return false;
  }

  private boolean _jspx_meth_menu_005fcategory_005f3(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:category
    org.apache.jsp.tag.web.menu.category_tagx _jspx_th_menu_005fcategory_005f3 = new org.apache.jsp.tag.web.menu.category_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f3);
    _jspx_th_menu_005fcategory_005f3.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fcategory_005f3.setParent(_jspx_th_sec_005fauthorize_005f3);
    // /WEB-INF/views/menu.jspx(26,79) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f3.setZ("3Bl8fDflQR4X9EuHlmX0BHno41Y=");
    // /WEB-INF/views/menu.jspx(26,79) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f3.setId("c_metricgoal");
    _jspx_th_menu_005fcategory_005f3.setJspBody(new Helper( 4, _jspx_page_context, _jspx_th_menu_005fcategory_005f3, null));
    _jspx_th_menu_005fcategory_005f3.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f3);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f7(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f7 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f7);
    _jspx_th_menu_005fitem_005f7.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f7.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(27,138) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f7.setZ("4RjfkifF8yLAzAup/W5KgBLlgcQ=");
    // /WEB-INF/views/menu.jspx(27,138) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f7.setUrl("/metricgoals?form");
    // /WEB-INF/views/menu.jspx(27,138) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f7.setMessageCode("global_menu_new");
    // /WEB-INF/views/menu.jspx(27,138) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f7.setId("i_metricgoal_new");
    _jspx_th_menu_005fitem_005f7.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f7);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f8(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f8 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f8);
    _jspx_th_menu_005fitem_005f8.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f8.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(28,189) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f8.setZ("YXqDlO8IBWArYzElcJmyF5P3d90=");
    // /WEB-INF/views/menu.jspx(28,189) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f8.setUrl((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("/metricgoals?page=1&size=${empty param.size ? 10 : param.size}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/menu.jspx(28,189) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f8.setMessageCode("global_menu_list");
    // /WEB-INF/views/menu.jspx(28,189) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f8.setId("i_metricgoal_list");
    _jspx_th_menu_005fitem_005f8.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f8);
    return false;
  }

  private boolean _jspx_meth_menu_005fcategory_005f4(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:category
    org.apache.jsp.tag.web.menu.category_tagx _jspx_th_menu_005fcategory_005f4 = new org.apache.jsp.tag.web.menu.category_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f4);
    _jspx_th_menu_005fcategory_005f4.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fcategory_005f4.setParent(_jspx_th_sec_005fauthorize_005f3);
    // /WEB-INF/views/menu.jspx(30,79) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f4.setZ("2Y/jECB9g2wQSpe9uEleBKsnriw=");
    // /WEB-INF/views/menu.jspx(30,79) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f4.setId("c_goalstatus");
    _jspx_th_menu_005fcategory_005f4.setJspBody(new Helper( 5, _jspx_page_context, _jspx_th_menu_005fcategory_005f4, null));
    _jspx_th_menu_005fcategory_005f4.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f4);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f9(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f9 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f9);
    _jspx_th_menu_005fitem_005f9.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f9.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(31,139) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f9.setZ("A6o5xzj7XPK9/h/fjlSoAamO7C4=");
    // /WEB-INF/views/menu.jspx(31,139) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f9.setUrl("/goalstatuses?form");
    // /WEB-INF/views/menu.jspx(31,139) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f9.setMessageCode("global_menu_new");
    // /WEB-INF/views/menu.jspx(31,139) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f9.setId("i_goalstatus_new");
    _jspx_th_menu_005fitem_005f9.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f9);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f10(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f10 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f10);
    _jspx_th_menu_005fitem_005f10.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f10.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(32,190) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f10.setZ("4Me0tI8EE8PzT8lmBzuGkyhhGQI=");
    // /WEB-INF/views/menu.jspx(32,190) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f10.setUrl((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("/goalstatuses?page=1&size=${empty param.size ? 10 : param.size}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/menu.jspx(32,190) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f10.setMessageCode("global_menu_list");
    // /WEB-INF/views/menu.jspx(32,190) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f10.setId("i_goalstatus_list");
    _jspx_th_menu_005fitem_005f10.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f10);
    return false;
  }

  private boolean _jspx_meth_sec_005fauthorize_005f4(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec:authorize
    org.springframework.security.taglibs.authz.JspAuthorizeTag _jspx_th_sec_005fauthorize_005f4 = (org.springframework.security.taglibs.authz.JspAuthorizeTag) _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted_005fnobody.get(org.springframework.security.taglibs.authz.JspAuthorizeTag.class);
    _jspx_th_sec_005fauthorize_005f4.setPageContext(_jspx_page_context);
    _jspx_th_sec_005fauthorize_005f4.setParent(new javax.servlet.jsp.tagext.TagAdapter((javax.servlet.jsp.tagext.SimpleTag) _jspx_parent));
    // /WEB-INF/views/menu.jspx(33,59) name = ifAllGranted type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sec_005fauthorize_005f4.setIfAllGranted("ROLE_ADMIN");
    int _jspx_eval_sec_005fauthorize_005f4 = _jspx_th_sec_005fauthorize_005f4.doStartTag();
    if (_jspx_th_sec_005fauthorize_005f4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted_005fnobody.reuse(_jspx_th_sec_005fauthorize_005f4);
      throw new SkipPageException();
    }
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted_005fnobody.reuse(_jspx_th_sec_005fauthorize_005f4);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f11(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f11 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f11);
    _jspx_th_menu_005fitem_005f11.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f11.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(34,170) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f11.setZ("8ZkGpymuTjJxaT+Re4dICzdEzX8=");
    // /WEB-INF/views/menu.jspx(34,170) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f11.setUrl("/goalstatuses?find=ByChallenger&form");
    // /WEB-INF/views/menu.jspx(34,170) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f11.setMessageCode("global_menu_find");
    // /WEB-INF/views/menu.jspx(34,170) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f11.setId("fi_goalstatus_challenger");
    _jspx_th_menu_005fitem_005f11.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f11);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f12(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f12 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f12);
    _jspx_th_menu_005fitem_005f12.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f12.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(35,158) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f12.setZ("XkRiJ/AUK7BH/joFIMIZ7D20Ljk=");
    // /WEB-INF/views/menu.jspx(35,158) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f12.setUrl("/goalstatuses?find=ByGoal&form");
    // /WEB-INF/views/menu.jspx(35,158) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f12.setMessageCode("global_menu_find");
    // /WEB-INF/views/menu.jspx(35,158) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f12.setId("fi_goalstatus_goal");
    _jspx_th_menu_005fitem_005f12.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f12);
    return false;
  }

  private boolean _jspx_meth_menu_005fcategory_005f5(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:category
    org.apache.jsp.tag.web.menu.category_tagx _jspx_th_menu_005fcategory_005f5 = new org.apache.jsp.tag.web.menu.category_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f5);
    _jspx_th_menu_005fcategory_005f5.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fcategory_005f5.setParent(_jspx_th_sec_005fauthorize_005f3);
    // /WEB-INF/views/menu.jspx(37,78) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f5.setZ("WwtPos1+dPGo/hSL5I+WXR0orJ8=");
    // /WEB-INF/views/menu.jspx(37,78) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f5.setId("c_challenge");
    _jspx_th_menu_005fcategory_005f5.setJspBody(new Helper( 6, _jspx_page_context, _jspx_th_menu_005fcategory_005f5, null));
    _jspx_th_menu_005fcategory_005f5.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f5);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f13(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f13 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f13);
    _jspx_th_menu_005fitem_005f13.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f13.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(38,136) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f13.setZ("oN1rSqB466vFXaUaeHYLJE3ISEg=");
    // /WEB-INF/views/menu.jspx(38,136) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f13.setUrl("/challenges?form");
    // /WEB-INF/views/menu.jspx(38,136) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f13.setMessageCode("global_menu_new");
    // /WEB-INF/views/menu.jspx(38,136) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f13.setId("i_challenge_new");
    _jspx_th_menu_005fitem_005f13.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f13);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f14(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f14 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f14);
    _jspx_th_menu_005fitem_005f14.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f14.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(39,187) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f14.setZ("JuvLtN3cfBfSg/4TBW/W6Hg5+LE=");
    // /WEB-INF/views/menu.jspx(39,187) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f14.setUrl((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("/challenges?page=1&size=${empty param.size ? 10 : param.size}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/views/menu.jspx(39,187) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f14.setMessageCode("global_menu_list");
    // /WEB-INF/views/menu.jspx(39,187) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f14.setId("i_challenge_list");
    _jspx_th_menu_005fitem_005f14.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f14);
    return false;
  }

  private boolean _jspx_meth_sec_005fauthorize_005f5(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec:authorize
    org.springframework.security.taglibs.authz.JspAuthorizeTag _jspx_th_sec_005fauthorize_005f5 = (org.springframework.security.taglibs.authz.JspAuthorizeTag) _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted.get(org.springframework.security.taglibs.authz.JspAuthorizeTag.class);
    _jspx_th_sec_005fauthorize_005f5.setPageContext(_jspx_page_context);
    _jspx_th_sec_005fauthorize_005f5.setParent(new javax.servlet.jsp.tagext.TagAdapter((javax.servlet.jsp.tagext.SimpleTag) _jspx_parent));
    // /WEB-INF/views/menu.jspx(40,58) name = ifAllGranted type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sec_005fauthorize_005f5.setIfAllGranted("ROLE_ADMIN");
    int _jspx_eval_sec_005fauthorize_005f5 = _jspx_th_sec_005fauthorize_005f5.doStartTag();
    if (_jspx_eval_sec_005fauthorize_005f5 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_meth_menu_005fitem_005f15(_jspx_th_sec_005fauthorize_005f5, _jspx_page_context))
        return true;
    }
    if (_jspx_th_sec_005fauthorize_005f5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted.reuse(_jspx_th_sec_005fauthorize_005f5);
      throw new SkipPageException();
    }
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005fifAllGranted.reuse(_jspx_th_sec_005fauthorize_005f5);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f15(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f5, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f15 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f15);
    _jspx_th_menu_005fitem_005f15.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f15.setParent(_jspx_th_sec_005fauthorize_005f5);
    // /WEB-INF/views/menu.jspx(41,161) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f15.setZ("S8RccSRsbNF2n5BufnW+nj/vjuE=");
    // /WEB-INF/views/menu.jspx(41,161) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f15.setUrl("/challenges?find=ByOwner&form");
    // /WEB-INF/views/menu.jspx(41,161) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f15.setMessageCode("global_menu_find");
    // /WEB-INF/views/menu.jspx(41,161) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f15.setId("fi_challenge_owner");
    _jspx_th_menu_005fitem_005f15.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f15);
    return false;
  }

  private boolean _jspx_meth_menu_005fcategory_005f6(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:category
    org.apache.jsp.tag.web.menu.category_tagx _jspx_th_menu_005fcategory_005f6 = new org.apache.jsp.tag.web.menu.category_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f6);
    _jspx_th_menu_005fcategory_005f6.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fcategory_005f6.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(45,69) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f6.setZ("HjmI4Chx/kBYfS/6HjBLbCA+XpU=");
    // /WEB-INF/views/menu.jspx(45,69) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fcategory_005f6.setId("c_view");
    _jspx_th_menu_005fcategory_005f6.setJspBody(new Helper( 7, _jspx_page_context, _jspx_th_menu_005fcategory_005f6, null));
    _jspx_th_menu_005fcategory_005f6.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fcategory_005f6);
    return false;
  }

  private boolean _jspx_meth_menu_005fitem_005f16(javax.servlet.jsp.tagext.JspTag _jspx_parent, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  menu:item
    org.apache.jsp.tag.web.menu.item_tagx _jspx_th_menu_005fitem_005f16 = new org.apache.jsp.tag.web.menu.item_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f16);
    _jspx_th_menu_005fitem_005f16.setJspContext(_jspx_page_context);
    _jspx_th_menu_005fitem_005f16.setParent(_jspx_parent);
    // /WEB-INF/views/menu.jspx(46,158) name = z type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f16.setZ("JWy5B/C4tzw59gkbdOzGQbJL2WY=");
    // /WEB-INF/views/menu.jspx(46,158) name = url type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f16.setUrl("/visualization/chart1");
    // /WEB-INF/views/menu.jspx(46,158) name = messageCode type = java.lang.String reqTime = true required = false fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f16.setMessageCode("global_generic");
    // /WEB-INF/views/menu.jspx(46,158) name = id type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_menu_005fitem_005f16.setId("i_view__visualizationvisualization_id");
    _jspx_th_menu_005fitem_005f16.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_menu_005fitem_005f16);
    return false;
  }

  private class Helper
      extends org.apache.jasper.runtime.JspFragmentHelper
  {
    private javax.servlet.jsp.tagext.JspTag _jspx_parent;
    private int[] _jspx_push_body_count;

    public Helper( int discriminator, JspContext jspContext, javax.servlet.jsp.tagext.JspTag _jspx_parent, int[] _jspx_push_body_count ) {
      super( discriminator, jspContext, _jspx_parent );
      this._jspx_parent = _jspx_parent;
      this._jspx_push_body_count = _jspx_push_body_count;
    }
    public boolean invoke0( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_menu_005fcategory_005f0(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_sec_005fauthorize_005f3(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fcategory_005f6(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public boolean invoke1( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_sec_005fauthorize_005f0(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_sec_005fauthorize_005f1(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_sec_005fauthorize_005f2(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public boolean invoke2( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_menu_005fitem_005f3(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fitem_005f4(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public boolean invoke3( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_menu_005fitem_005f5(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fitem_005f6(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public boolean invoke4( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_menu_005fitem_005f7(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fitem_005f8(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public boolean invoke5( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_menu_005fitem_005f9(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fitem_005f10(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_sec_005fauthorize_005f4(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fitem_005f11(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fitem_005f12(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public boolean invoke6( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_menu_005fitem_005f13(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_menu_005fitem_005f14(_jspx_parent, _jspx_page_context))
        return true;
      if (_jspx_meth_sec_005fauthorize_005f5(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public boolean invoke7( JspWriter out ) 
      throws Throwable
    {
      if (_jspx_meth_menu_005fitem_005f16(_jspx_parent, _jspx_page_context))
        return true;
      return false;
    }
    public void invoke( java.io.Writer writer )
      throws JspException
    {
      JspWriter out = null;
      if( writer != null ) {
        out = this.jspContext.pushBody(writer);
      } else {
        out = this.jspContext.getOut();
      }
      try {
        this.jspContext.getELContext().putContext(JspContext.class,this.jspContext);
        switch( this.discriminator ) {
          case 0:
            invoke0( out );
            break;
          case 1:
            invoke1( out );
            break;
          case 2:
            invoke2( out );
            break;
          case 3:
            invoke3( out );
            break;
          case 4:
            invoke4( out );
            break;
          case 5:
            invoke5( out );
            break;
          case 6:
            invoke6( out );
            break;
          case 7:
            invoke7( out );
            break;
        }
      }
      catch( Throwable e ) {
        if (e instanceof SkipPageException)
            throw (SkipPageException) e;
        throw new JspException( e );
      }
      finally {
        if( writer != null ) {
          this.jspContext.popBody();
        }
      }
    }
  }
}
