package org.apache.jsp.WEB_002dINF.views.visualization;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class visual_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>\r\n");
      out.write("<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js\"></script>\r\n");
      out.write("<!-- initialize the chart api -->\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\"> \r\n");
      out.write("\r\n");
      out.write(" google.load('visualization', '1.0', {'packages':['corechart']}); \r\n");
      out.write(" google.setOnLoadCallback(drawChart); \r\n");
      out.write(" \r\n");
      out.write(" function drawChart() {  \r\n");
      out.write("                         \r\n");
      out.write("         var patient_data = new google.visualization.DataTable();                \r\n");
      out.write("         patient_data.addColumn('date', 'Record Date');\r\n");
      out.write("         patient_data.addColumn('number', 'Blood Glucose'); \r\n");
      out.write("         patient_data.addColumn('number', 'Insulin'); \r\n");
      out.write("         patient_data.addRows([          \r\n");
      out.write("                      [new Date(2012,9,6),7,13],\r\n");
      out.write("                      [new Date(2012,9,5),23,3],\r\n");
      out.write("                      [new Date(2012,9,4),14,43],\r\n");
      out.write("                      [new Date(2012,9,3),7,23],\r\n");
      out.write("                      [new Date(2012,9,2),57,33]\r\n");
      out.write("\r\n");
      out.write("         ]); \r\n");
      out.write("         \r\n");
      out.write("         var patient_options = {'title':'Patient Info', \r\n");
      out.write("                          vAxis: {title: 'Unit',  titleTextStyle: {color: 'black'}},\r\n");
      out.write("                         'width':500,                       \r\n");
      out.write("                         'height':400}; \r\n");
      out.write("         var patient_chart = new google.visualization.LineChart(document.getElementById('patient_chart')); \r\n");
      out.write("         patient_chart .draw(patient_data, patient_options); \r\n");
      out.write("         \r\n");
      out.write("                                        \r\n");
      out.write("                        \r\n");
      out.write("                \r\n");
      out.write("         var workout_data = google.visualization.arrayToDataTable([ \r\n");
      out.write("                                ['Date', 'Workout'], \r\n");
      out.write("                                ['2012-9-3',  123],['2012-9-4',  67],['2012-9-5',  84]\r\n");
      out.write("                                 ]); \r\n");
      out.write("         var workout_chart_options = {'title':'Workout information', \r\n");
      out.write("                         vAxis: {title: 'Time',  titleTextStyle: {color: 'black'}},\r\n");
      out.write("                         'width':500,                       \r\n");
      out.write("                         'height':400}; \r\n");
      out.write("         var workout_chart = new google.visualization.ColumnChart(document.getElementById('workout_chart')); \r\n");
      out.write("         workout_chart.draw(workout_data, workout_chart_options);   \r\n");
      out.write("         \r\n");
      out.write("         \r\n");
      out.write("         \r\n");
      out.write("         \r\n");
      out.write("         google.load('visualization', '1.0', {'packages':['corechart']}); \r\n");
      out.write("         google.setOnLoadCallback(drawChart); \r\n");
      out.write("         \r\n");
      out.write("       \r\n");
      out.write("                              \r\n");
      out.write("        \t var data = new google.visualization.DataTable();                 \r\n");
      out.write("              \r\n");
      out.write("        \t data.addColumn('string', 'Energy');  \r\n");
      out.write("        \t data.addColumn('number', 'Calorie');  \r\n");
      out.write("        \t data.addRows([         \r\n");
      out.write("        \t\t\t\t['Intake', 300], \r\n");
      out.write("        \t\t\t\t['Consuming', 179], \r\n");
      out.write("                 ]); \r\n");
      out.write("                 \r\n");
      out.write("        \t var options = {'title':'Calorie',                        \r\n");
      out.write("        \t\t\t 'width':500,                       \r\n");
      out.write("        \t\t\t 'height':400};  \r\n");
      out.write("        \t var chart = new google.visualization.PieChart(document.getElementById('chart_div')); \r\n");
      out.write("        \t chart.draw(data, options);        \r\n");
      out.write("                 \r\n");
      out.write("         \r\n");
      out.write("         \r\n");
      out.write(" }\r\n");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("$(document).ready(function(){\r\n");
      out.write("        \r\n");
      out.write("\t $(\"#chart_div\").hide();\r\n");
      out.write("\t $(\"#workout_chart\").hide();\r\n");
      out.write("\t \r\n");
      out.write("        });\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("$(document).ready(function(){\r\n");
      out.write("        $(\"#piechartButton\").click(function(){\r\n");
      out.write("        $(\"#patient_chart\").hide();\t\r\n");
      out.write("        $(\"#workout_chart\").hide();\r\n");
      out.write("        $(\"#chart_div\").show();\r\n");
      out.write("        });\r\n");
      out.write("        });\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("$(document).ready(function(){\r\n");
      out.write("        $(\"#linechartButton\").click(function(){\r\n");
      out.write("        \t\r\n");
      out.write("       \r\n");
      out.write("        $(\"#chart_div\").hide();\r\n");
      out.write("        $(\"#workout_chart\").hide();\r\n");
      out.write("        $(\"#patient_chart\").show();\r\n");
      out.write("        });\r\n");
      out.write("        });\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("$(document).ready(function(){\r\n");
      out.write("        $(\"#barchartButton\").click(function(){\r\n");
      out.write("        \t\r\n");
      out.write("        $(\"#chart_div\").hide();\r\n");
      out.write("        $(\"#patient_chart\").hide();\r\n");
      out.write("        $(\"#workout_chart\").show();\r\n");
      out.write("       \r\n");
      out.write("        });\r\n");
      out.write("        });\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<title>View the workout records</title>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("<div id=\"patient_chart\"></div> \r\n");
      out.write("<div id=\"workout_chart\"></div>\r\n");
      out.write("<div id=\"chart_div\"></div> \r\n");
      out.write("<input type=\"button\" id=\"linechartButton\" value=\"Line Chart\">\r\n");
      out.write("<input type=\"button\" id=\"piechartButton\" value=\"Pie Chart\">\r\n");
      out.write("<input type=\"button\" id=\"barchartButton\" value=\"Bar Chart\">\r\n");
      out.write("\r\n");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${ number}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
      out.write(" \r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
